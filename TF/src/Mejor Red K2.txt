Mejor red bayesiana por entropia y fuerza bruta
0 1 1 1 0 0 0 
1 0 1 1 0 0 0 
1 1 0 1 0 0 0 
1 0 0 0 0 0 0 
0 0 0 0 0 0 0 
1 1 1 0 0 0 0 
1 0 0 0 0 0 0 
mejor entropia FB por conteo: -7491.288661544521
mejor entropia FB por prob: -502.02061348507425
akaike FB del grafo: -6137.288661544521

Mejor red bayesiana por akaike y fuerza bruta:
0 0 0 1 0 0 0 
0 0 1 1 0 0 0 
0 0 0 0 0 0 0 
0 0 0 0 0 0 0 
0 0 0 0 0 0 0 
1 0 1 0 0 0 0 
0 0 0 0 0 0 0 
mejor akaike: -6883.122894444551
entropia por conteo: -7271.122894444551
entropia por prob: -145.5587952979049