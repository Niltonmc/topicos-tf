import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BayesianInferecer {
    private Dataset adjuntDataset;
    private double alphaValue;
    List<Integer> combinatories;
    int[][] combinatoriesMatrix;
    int[] distributionCountsForEntrophy;

    HashMap<String,double[]> factorsHashMap;//OPTIMIZACION
    HashMap<String,int[]> distributionCountsHashMap;//OPTIMIZACION


    //int[] combinatoriesRowSpeed;

    public BayesianInferecer(){

    }

    public BayesianInferecer(Dataset dataset, double value){
        adjuntDataset = dataset;
        alphaValue = value;
        factorsHashMap = new HashMap<String, double[]>();//OPTIMIZACION
        distributionCountsHashMap = new HashMap<String, int[]>();//OPTIMIZACION
    }
    public double[] CalculateMarginalDistribution(int variableIndex){
        int[] variableToEvaluate = new int[1];
        variableToEvaluate[0] = variableIndex;
        int[] variableValue = new int[1];
        double[] distributionResults = new double[adjuntDataset.GetVariableCardinality(variableIndex)];

        distributionCountsForEntrophy = new int[adjuntDataset.GetVariableCardinality(variableIndex)];

        String keyHashMap = Integer.toString(variableIndex);//OPTIMIZACION
        if(factorsHashMap.containsKey(keyHashMap)){//OPTIMIZACION
            distributionCountsForEntrophy = distributionCountsHashMap.get(keyHashMap);//OPTIMIZACION
            distributionResults = factorsHashMap.get(keyHashMap);//OPTIMIZACION
        }else {//OPTIMIZACION
            for (int i = 0; i < distributionResults.length; i++) {
                variableValue[0] = i;
                distributionCountsForEntrophy[i] = adjuntDataset.CountTuples(variableToEvaluate, variableValue);
                distributionResults[i] = (double) (distributionCountsForEntrophy[i] + alphaValue) /
                        (adjuntDataset.GetTuplesNumbs() + adjuntDataset.GetVariableCardinality(variableIndex) * alphaValue);
            }
            distributionCountsHashMap.put(keyHashMap,distributionCountsForEntrophy);//OPTIMIZACION
            factorsHashMap.put(keyHashMap,distributionResults);//OPTIMIZACION
        }//OPTIMIZACION
        return distributionResults;
    }

    public double CalculateSpecificMarginalDistribution(int variableIndex, int varValue) {
        int[] variableToEvaluate = new int[1];
        variableToEvaluate[0] = variableIndex;
        int[] variableValue = new int[1];
        variableValue[0] = varValue;
        double distributionResult;
        distributionResult = (adjuntDataset.CountTuples(variableToEvaluate, variableValue) + alphaValue) /
                (adjuntDataset.GetTuplesNumbs() + adjuntDataset.GetVariableCardinality(variableIndex) * alphaValue);

        return distributionResult;
    }
/*
    public double[] CalculateJointDistribution(int[] variablesIndex){
        int distributionLength = 1;
        for(int i = 0; i< variablesIndex.length;i++){
            distributionLength = distributionLength * adjuntDataset.GetVariableCardinality(variablesIndex[i]);
        }
        double[] distributionResults = new double[distributionLength];
        combinatories = new ArrayList<Integer>();
        //List<Integer> combinatories = new ArrayList<Integer>();
        int currentVariableIndex = 0;
        for(int i = 0; i<variablesIndex.length;i++){
            currentVariableIndex = 0;
            for(int j = 0;
                j < distributionLength / adjuntDataset.GetVariableCardinality(variablesIndex[i]); j++){
                    for(int k = 0; k < adjuntDataset.GetVariableCardinality(variablesIndex[i]);k++){
                        combinatories.addAll(RepeatNumber(k,adjuntDataset.GetVariableCardinality(variablesIndex[0])*i));
                    }
            }
        }
       for(int i = 0; i<combinatories.size();i++){
           System.out.print(combinatories.get(i) +"\t");
       }

        return distributionResults;
    }
*/
    public double CalculateSpecificJointDistribution(int[] variablesIndex, int[] values) {
        double distributionResult;
        int distributionLength = 1;
        for(int i = 0; i < variablesIndex.length;i++){
            distributionLength = distributionLength*adjuntDataset.GetVariableCardinality(variablesIndex[i]);
        }
        distributionResult = (adjuntDataset.CountTuples(variablesIndex, values) + alphaValue) /
                (adjuntDataset.GetTuplesNumbs() + distributionLength * alphaValue);

        return distributionResult;
    }

    public double[] CalculateJointDistribution2(int[] variablesIndex){
        int distributionLength = 1;
        for(int i = 0; i< variablesIndex.length;i++){
            distributionLength = distributionLength * adjuntDataset.GetVariableCardinality(variablesIndex[i]);
            //System.out.println(distributionLength);
        }
        System.out.println(distributionLength);
        double[] distributionResults = new double[distributionLength];

        distributionCountsForEntrophy = new int[distributionLength];

        int firstVariableCardinality = adjuntDataset.GetVariableCardinality(variablesIndex[0]);
        ConstructCombinatories(variablesIndex,distributionLength,firstVariableCardinality);

        for(int i = 0; i <distributionLength;i++){
            int[] variablesValues = new int[variablesIndex.length];
            for(int j = 0; j < variablesIndex.length;j++){
                variablesValues[j] = combinatoriesMatrix[i][j];
            }

            distributionCountsForEntrophy[i] = adjuntDataset.CountTuples(variablesIndex,variablesValues);

            distributionResults[i] = (double) (distributionCountsForEntrophy[i] + alphaValue) /
                    (adjuntDataset.GetTuplesNumbs() + distributionLength*alphaValue);
        }

        return distributionResults;
    }
    private void PrintCombinatories(List<Integer> data){
        for(int i = 0; i<data.size();i++){
            System.out.print(data.get(i) +"\t");
        }
    }
    public int[][] GetCombinatories(){
        return combinatoriesMatrix;
    }
    private void ConstructCombinatories(int[] variablesIndex, int distributionLength, int firstVariableCardinality){
        combinatoriesMatrix = new int[distributionLength][variablesIndex.length];
        int changeSpeed = 0;
        int currentValue = 0;
        int trueSpeed = 0;
        for(int columna = 0; columna < variablesIndex.length;columna++){
            currentValue = 0;
            if(columna == 1){
                changeSpeed = 1;
            }
            if(columna>0) {
                changeSpeed = changeSpeed * adjuntDataset.GetVariableCardinality(variablesIndex[columna - 1]);
            }
            if(columna == 0){
                changeSpeed = 0;
            }
            trueSpeed = changeSpeed;
            for(int fila = 0; fila<distributionLength;fila++){
                combinatoriesMatrix[fila][columna] = currentValue;
                if(trueSpeed%(fila+1) == 0 && (fila+1)>=trueSpeed) {
                    currentValue = currentValue + 1;
                    trueSpeed = trueSpeed + changeSpeed;
                }
                if (currentValue >= adjuntDataset.GetVariableCardinality(variablesIndex[columna])) {
                    currentValue = 0;
                }
            }
        }



        /*
        for(int fila = 0; fila<distributionLength;fila++){
        for(int columna = 0; columna < variablesIndex.length;columna++){

                System.out.print(combinatoriesMatrix[fila][columna]+"\t");
            }
            System.out.println();
        }
        */
    }

    public int[][] ConstructAnGetCombinatories(int[] variablesIndex) {
        int distributionLength = 1;
        for(int i = 0; i< variablesIndex.length;i++){
            distributionLength = distributionLength * adjuntDataset.GetVariableCardinality(variablesIndex[i]);
        }
        System.out.println(distributionLength);

        combinatoriesMatrix = new int[distributionLength][variablesIndex.length];
        int changeSpeed = 0;
        int currentValue = 0;
        int trueSpeed = 0;
        for (int columna = 0; columna < variablesIndex.length; columna++) {
            currentValue = 0;
            if (columna == 1) {
                changeSpeed = 1;
            }
            if (columna > 0) {
                changeSpeed = changeSpeed * adjuntDataset.GetVariableCardinality(variablesIndex[columna - 1]);
            }
            if (columna == 0) {
                changeSpeed = 0;
            }
            trueSpeed = changeSpeed;
            for (int fila = 0; fila < distributionLength; fila++) {
                combinatoriesMatrix[fila][columna] = currentValue;
                if (trueSpeed % (fila + 1) == 0 && (fila + 1) >= trueSpeed) {
                    currentValue = currentValue + 1;
                    trueSpeed = trueSpeed + changeSpeed;
                }
                if (currentValue >= adjuntDataset.GetVariableCardinality(variablesIndex[columna])) {
                    currentValue = 0;
                }
            }
        }
        return combinatoriesMatrix;
    }

    /*
    public double[] CalculateConditionalDistribution(int[] variablesIndex){
            int distributionLength = 1;
            for(int i = 0; i< variablesIndex.length;i++){
                distributionLength = distributionLength * adjuntDataset.GetVariableCardinality(variablesIndex[i]);
                //System.out.println(distributionLength);
            }
            //System.out.println(distributionLength);
            double[] distributionResults = new double[distributionLength];
            int firstVariableCardinality = adjuntDataset.GetVariableCardinality(variablesIndex[0]);
            ConstructCombinatories(variablesIndex,distributionLength,firstVariableCardinality);
            int[] varIndex = new int[1];
            varIndex[0] = variablesIndex[0];
            int[] varValue = new int[1];

            for(int i = 0; i <distributionLength;i++){
                int[] variablesValues = new int[variablesIndex.length];
                for(int j = 0; j < variablesIndex.length;j++){
                    variablesValues[j] = combinatoriesMatrix[i][j];
                }
                varValue[0] = variablesValues[0];
                distributionResults[i] = (double) ((adjuntDataset.CountTuples(variablesIndex,variablesValues) + alphaValue) /
                        (adjuntDataset.GetTuplesNumbs() + distributionLength*alphaValue))/
                        ((adjuntDataset.CountTuples(varIndex,varValue) + alphaValue)/
                                (adjuntDataset.GetTuplesNumbs() + adjuntDataset.GetVariableCardinality(0)*alphaValue));
            }
            return distributionResults;
        }
*/
    public double[] CalculateConditionalDistributionWithNormalization(int[] variablesIndex){
        int distributionLength = 1;
        String keyHashMap = "";//OPTIMIZACION
        for(int i = 0; i< variablesIndex.length;i++){
            distributionLength = distributionLength * adjuntDataset.GetVariableCardinality(variablesIndex[i]);
            keyHashMap = keyHashMap + Integer.toString(variablesIndex[i]);//OPTIMIZACION
            //System.out.println(distributionLength);
        }
        //System.out.println(distributionLength);
        double[] distributionResults = new double[distributionLength];

        distributionCountsForEntrophy = new int[distributionLength];

        int firstVariableCardinality = adjuntDataset.GetVariableCardinality(variablesIndex[0]);
        ConstructCombinatories(variablesIndex,distributionLength,firstVariableCardinality);

        int[] varIndex = new int[1];
        varIndex[0] = variablesIndex[0];
        int[] varValue = new int[1];


        if(factorsHashMap.containsKey(keyHashMap)){//OPTIMIZACION
            distributionCountsForEntrophy = distributionCountsHashMap.get(keyHashMap);//OPTIMIZACION
            distributionResults = factorsHashMap.get(keyHashMap);//OPTIMIZACION
        }else {//OPTIMIZACION
            for (int i = 0; i < distributionLength; i++) {
                int[] variablesValues = new int[variablesIndex.length];
                for (int j = 0; j < variablesIndex.length; j++) {
                    variablesValues[j] = combinatoriesMatrix[i][j];
                }
                varValue[0] = variablesValues[0];

                distributionCountsForEntrophy[i] = adjuntDataset.CountTuples(variablesIndex, variablesValues);

                distributionResults[i] = (double) ((distributionCountsForEntrophy[i] + alphaValue) /
                        (adjuntDataset.GetTuplesNumbs() + distributionLength * alphaValue));
            }
            distributionResults = NormalizeDistributionResults(distributionResults,
                    adjuntDataset.GetVariableCardinality(variablesIndex[1]));

            distributionCountsHashMap.put(keyHashMap,distributionCountsForEntrophy);//OPTIMIZACION
            factorsHashMap.put(keyHashMap,distributionResults);//OPTIMIZACION
        }
        return distributionResults;
    }

    public double CalculateSpecificalConditionalDistributionWithNormalization(int[] variablesIndex, int[] varValues) {
        int distributionLength = 1;
        int combinationPosition = 0;
        for (int i = 0; i < variablesIndex.length; i++) {
            distributionLength = distributionLength * adjuntDataset.GetVariableCardinality(variablesIndex[i]);
            //System.out.println(distributionLength);
        }
        //System.out.println(distributionLength);
        double[] distributionResults = new double[distributionLength];
        int firstVariableCardinality = adjuntDataset.GetVariableCardinality(variablesIndex[0]);
        ConstructCombinatories(variablesIndex, distributionLength, firstVariableCardinality);

        int[] varIndex = new int[1];
        varIndex[0] = variablesIndex[0];
        int[] varValue = new int[1];

        for (int i = 0; i < distributionLength; i++) {
            int[] variablesValues = new int[variablesIndex.length];
            for (int j = 0; j < variablesIndex.length; j++) {
                variablesValues[j] = combinatoriesMatrix[i][j];
            }
            if(variablesValues == varValues){
                combinationPosition = i;
            }
            varValue[0] = variablesValues[0];
            distributionResults[i] = (double) ((adjuntDataset.CountTuples(variablesIndex, variablesValues) + alphaValue) /
                    (adjuntDataset.GetTuplesNumbs() + distributionLength * alphaValue));
        }
        distributionResults = NormalizeDistributionResults(distributionResults,
                adjuntDataset.GetVariableCardinality(variablesIndex[1]));
        return distributionResults[combinationPosition];
    }
/*
    public double CalculateSpecificConditionalDistributionWithNormalization(int[] variablesIndex, int[] variableValues){
        int firstVariableCard = adjuntDataset.GetVariableCardinality(variablesIndex[0]);
        int distributionLength = 1;
        for(int i = 0; i< variablesIndex.length;i++){
            distributionLength = distributionLength * adjuntDataset.GetVariableCardinality(variablesIndex[i]);
        }
        double distributionResult = 0;
        double[] distributionResults = new double[distributionLength];
        int[] varValue = new int[variablesIndex.length];

        for(int i = 0; i < firstVariableCard;i++){
            varValue[0] = i;
            for(int j = 1; j < (variablesIndex.length-1);j++){
                varValue[j] = variableValues[j];
            }
            distributionResults[i] = (double) ((adjuntDataset.CountTuples(variablesIndex,varValue) + alphaValue) /
                    (adjuntDataset.GetTuplesNumbs() + distributionLength*alphaValue));
        }
        distributionResults =  NormalizeDistributionResults(distributionResults,firstVariableCard);
        //System.out.println(distributionResults);
        distributionResult = distributionResults[variableValues[0]];
        return distributionResult;
    }
*/
    private double[] NormalizeDistributionResults(double[] distributionResult, int consecuentCardinality) {
        double[] normalizeDistribution = distributionResult;
        int distributionGroups = normalizeDistribution.length / consecuentCardinality;
        double normalizedValue;
        for (int i = 0; i < consecuentCardinality; i++) {
            normalizedValue = 0;
            for (int j = 0; j < distributionGroups; j++) {
                normalizedValue = normalizedValue + normalizeDistribution[i * distributionGroups + j];
            }
            for (int j = 0; j < distributionGroups; j++) {
                normalizeDistribution[i * distributionGroups + j] = normalizeDistribution[i * distributionGroups + j] / normalizedValue;
            }
            //System.out.println("La normalizacion pesa: " + normalizedValue);
        }
        return normalizeDistribution;
    }
    public void SetAlphaValue(double val) {
        alphaValue = val;
    }

    public int[] GetDistributionCountForEntrophy(){
        return  distributionCountsForEntrophy;
    }
}
