public class FrequentistInferencer {
    private Dataset adjuntDataset;
    public FrequentistInferencer(Dataset dataset){
        adjuntDataset = dataset;
    }
    public float CalculateMarginalDistribution(int variableIndex , int value){
        float result = 0;
        int[] variableArray = new int[1];
        variableArray[0] = variableIndex;
        int[] valueArray = new int[1];
        valueArray[0] = value;
        result = (float)adjuntDataset.CountTuples(variableArray,valueArray)/adjuntDataset.GetTuplesNumbs();
        return result;
    }
    public float CalculateJointDistribution(int[] variablesIndex, int[] values){
        float result = 0;
        result = (float) adjuntDataset.CountTuples(variablesIndex,values)/adjuntDataset.GetTuplesNumbs();
        return result;
    }
    public float CalculateConditionalDistribution(int[] variablesIndex, int[] values){
        float result = 0;
        int[] variableArray = new int[1];
        variableArray[0] = variablesIndex[1];
        int[] valueArray = new int[1];
        valueArray[0] = values[1];
        result = (float) adjuntDataset.CountTuples(variablesIndex,values)/adjuntDataset.CountTuples(variableArray,valueArray);
        return result;
    }
}
