import javax.xml.crypto.Data;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {

    public static void PrintFactor(int[] varIndex, Dataset data, BayesianInferecer inferencer, double[] results, String inputText){
        //int[][] combinatories = inferencer.GetCombinatories();
        int[][] combinatories = inferencer.ConstructAnGetCombinatories(varIndex);
        int distributionLength = 1;
        System.out.print("I" + "\t\t\t\t");
        for(int i = 0; i< varIndex.length;i++){
            distributionLength = distributionLength * data.GetVariableCardinality(varIndex[i]);
            System.out.print(data.GetVariableName(varIndex[i]) + "\t\t\t\t");
        }
        System.out.print(inputText);
        System.out.println();
        for(int i = 0; i <distributionLength;i++) {
            int[] variablesValues = new int[varIndex.length];
            System.out.print(i + "\t\t\t\t");
            for (int j = 0; j < varIndex.length; j++) {
                variablesValues[j] = combinatories[i][j];
            }
            for(int j = 0; j<variablesValues.length;j++){
                System.out.print(data.GetVariableValueName(varIndex[j],variablesValues[j]) + "\t\t\t\t");
                //System.out.print(variablesValues[j] + "\t\t\t\t");
            }
            System.out.print(String.format("%.05f",results[i]));
            System.out.println();
        }
    }

    public static void PrintMarginal(double[] results, int varIndex, String name, String input, Dataset data){
        System.out.print("I" + "\t\t\t\t");
        System.out.print(name + "\t\t\t\t");
        System.out.print(input + "\t\t\t\t");
        System.out.println();
        for(int i = 0; i<results.length;i++){
            System.out.println(i + "\t\t\t\t" + data.GetVariableValueName(varIndex,i) + "\t\t\t\t" + String.format("%.05f",results[i]));
        }
    }

    public static void HillClimbing(){

    }

    public static void main(String[] args) throws FileNotFoundException {
        boolean isExecutingProgram = true;
        String[] inputsTexts;
        String originalInput;
        String[] inputsStringValues;
        int[] variablesIndex;
        int[] variablesValues;
        Graph graph = new Graph();
        Dataset dataset = new Dataset();;
        ARFFReader arffReader = new ARFFReader();;
        BayesianInferecer bayesianInferecer = new BayesianInferecer();
        BayesianNetwork bayesianNetwork;
        List<String> listBayesianNetworksNames = new ArrayList<String>();
        List<BayesianNetwork> listBayesianNetworks = new ArrayList<BayesianNetwork>();
        double[] resultsDistribution;
        double resultDistribution;

        double bestEntrophyValue;
        double currentEntrophyValue;
        double bestAkaikeValue;
        double currentAkaikeValue;
        long bestEntrophyIndex;
        long bestAkaikeIndex;
        int[][] dagMatrix;

        BayesianNetworkReader bayesianNetworkReader = new BayesianNetworkReader();
        /*
        int[][] dataTestMatrix;
        int testClassIndex = 0;
        double testResultDistribution = 0;
         */

        while (isExecutingProgram){
            Scanner inputScanner= new Scanner(System.in);
            String inputText = inputScanner.nextLine();
           if(inputText.contains("load") && inputText.contains("data")){
               dataset = new Dataset();
               inputsTexts = inputText.split("=");
               String fileName = inputsTexts[1];
               arffReader = new ARFFReader(fileName,dataset);
               dataset = arffReader.ExecuteAutomaton();
               dataset.ConvertDataListToMatrix();

                bayesianInferecer = new BayesianInferecer(dataset,0);

               System.out.println("Se leyó con éxito el dataset.");
               dataset.CrossValidationMatrix(1);
               //int numb = Runtime.getRuntime().availableProcessors();
               //System.out.println(numb);
           }
            else if(inputText.contains("setalpha")){
                inputsTexts = inputText.split("=");
                bayesianInferecer.SetAlphaValue(Double.parseDouble(inputsTexts[1]));
                System.out.println("Se cambió con éxito el valor del hiperparámetro.");
            }
            else if(inputText.contains("P(") && !inputText.contains("|") && !inputText.contains(",")) {
               originalInput = inputText;
               inputText = inputText.replaceAll("\\)", "\\(");
               inputText = inputText.replaceAll("=", "\\(");
               inputsTexts = inputText.split("\\(");
               if (inputsTexts.length > 2) {
                   int varIndex = dataset.GetVariableIndex(inputsTexts[1]);
                   int varValue = dataset.GetVariableValue(inputsTexts[2], varIndex);
                   //int varValue = Integer.parseInt(inputsTexts[2]);
                   //System.out.println(varValue);
                   //System.out.println(varIndex);
                   resultDistribution = bayesianInferecer.CalculateSpecificMarginalDistribution(varIndex, varValue);
                   System.out.println("I" + "\t\t\t\t" + inputsTexts[1] + "\t\t\t\t" + originalInput);
                   System.out.println(varValue + "\t\t\t\t" + inputsTexts[2] + "\t\t\t\t" + String.format("%.05f",resultDistribution));
               } else {
                   int varIndex = dataset.GetVariableIndex(inputsTexts[1]);
                   //System.out.println(varIndex);
                   resultsDistribution = bayesianInferecer.CalculateMarginalDistribution(varIndex);
                   PrintMarginal(resultsDistribution, varIndex, inputsTexts[1], originalInput, dataset);
               }
           }
           else if(inputText.contains("P(") && !inputText.contains("|") && inputText.contains(",")) {
               originalInput = inputText;
               inputText = inputText.replaceAll("\\)","\\(");
               inputsTexts = inputText.split("\\(");
               if(inputText.contains("=")){
                   inputsStringValues = inputsTexts[1].split(",");
                   variablesIndex = new int[inputsStringValues.length];
                   variablesValues = new int[inputsStringValues.length];
                   for(int i = 0; i<inputsStringValues.length;i++){
                       String[] inputVarIndexAndValue = inputsStringValues[i].split("=");
                       variablesIndex[i] = dataset.GetVariableIndex(inputVarIndexAndValue[0]);
                       variablesValues[i] = dataset.GetVariableValue(inputVarIndexAndValue[1],variablesIndex[i]);
                   }
                   resultDistribution = bayesianInferecer.CalculateSpecificJointDistribution(variablesIndex,variablesValues);
                   System.out.print("I" + "\t\t\t\t");
                   for(int i = 0; i<variablesIndex.length;i++){
                       System.out.print(dataset.GetVariableName(variablesIndex[i])  + "\t\t\t\t");
                   }
                   System.out.print(originalInput);
                   System.out.println();
                   System.out.print("0" + "\t\t\t\t");
                   for(int i = 0; i<variablesIndex.length;i++){
                       System.out.print(dataset.GetVariableValueName(variablesIndex[i],variablesValues[i])  + "\t\t\t\t");
                   }
                   System.out.println(resultDistribution);
               }else{
                   inputsStringValues = inputsTexts[1].split(",");
                   variablesIndex = new int[inputsStringValues.length];
                   for(int i = 0;i<inputsStringValues.length;i++){
                       variablesIndex[i] = dataset.GetVariableIndex(inputsStringValues[i]);
                   }
                   resultsDistribution = bayesianInferecer.CalculateJointDistribution2(variablesIndex);
                   PrintFactor(variablesIndex,dataset,bayesianInferecer,resultsDistribution,originalInput);
               }
           }
           else if(inputText.contains("P(") && inputText.contains("|")) {
               originalInput = inputText;

               inputText = inputText.replaceAll("\\)","\\(");
               inputText = inputText.replaceAll("\\|","\\(");
               inputText = inputText.replaceAll(",","\\(");
               inputsTexts = inputText.split("\\(");

               if(inputText.contains("=")){
                   for(int i = 0;i<inputsTexts.length;i++){
                       System.out.println(inputsTexts[i]);
                   }
                   variablesIndex = new int[inputsTexts.length-1];
                   variablesValues = new int[inputsTexts.length-1];
                   for(int i = 0; i<(inputsTexts.length-1);i++){
                       String[] inputVarIndexAndValue = inputsTexts[i+1].split("=");
                       variablesIndex[i] = dataset.GetVariableIndex(inputVarIndexAndValue[0]);
                       //variablesValues[i] = Integer.parseInt(inputVarIndexAndValue[1]);
                       variablesValues[i] = dataset.GetVariableValue(inputVarIndexAndValue[1],variablesIndex[i]);
                   }
                   //resultsDistribution = bayesianInferecer.CalculateConditionalDistributionWithNormalization()
                   resultDistribution = bayesianInferecer.CalculateSpecificalConditionalDistributionWithNormalization(
                           variablesIndex,variablesValues);
                   /*
                   resultDistribution = bayesianInferecer.CalculateSpecificConditionalDistributionWithNormalization(
                           variablesIndex,variablesValues);
                    */
                   System.out.println("La distribución condicional para las variables es: " + String.format("%.05f",resultDistribution));
               }
               else{
                   variablesIndex = new int[inputsTexts.length-1];
                   for(int i = 0;i<(inputsTexts.length-1);i++){
                       variablesIndex[i] = dataset.GetVariableIndex(inputsTexts[i+1]);
                       //System.out.println(variablesIndex[i]);
                   }
                   resultsDistribution = bayesianInferecer.CalculateConditionalDistributionWithNormalization(variablesIndex);
                   PrintFactor(variablesIndex,dataset,bayesianInferecer,resultsDistribution,originalInput);
                   /*
                   for(int i = 0; i<resultsDistribution.length;i++){
                       System.out.println("La distribución condicional para las variables es: " + resultsDistribution[i]);
                   }
                    */
               }
           }
           else if(inputText.contains("load") && inputText.contains("graph")){
               graph = new Graph();
               inputsTexts = inputText.split("=");
               String fileName = inputsTexts[1];
               System.out.println(fileName);
               graph.ReadSaveConections(fileName);

           }
           else if(inputText.contains("redBayesiana")){
               inputText = inputText.replaceAll("\\)","\\(");
               inputText = inputText.replaceAll("\\|","\\(");
               inputText = inputText.replaceAll(",","\\(");
               inputText = inputText.replaceAll("=","\\(");
               inputsTexts = inputText.split("\\(");
               String bayesianName = inputsTexts[0];
               System.out.println("Se guardó la red bayesiana: " + inputsTexts[0]);
               listBayesianNetworksNames.add(bayesianName);
               bayesianNetwork = new BayesianNetwork(dataset,graph,bayesianInferecer);
               listBayesianNetworks.add(bayesianNetwork);
           }
           else if(inputText.contains("entropia")){
               inputsTexts = inputText.split("=");
               int index = 0;
               for(int i = 0; i<listBayesianNetworks.size();i++){
                   if(listBayesianNetworksNames.get(i).equals(inputsTexts[1])){
                       index = i;
                       break;
                   }
               }
               System.out.println("El valor de entropía es: " + listBayesianNetworks.get(index).CalculateEntropyByCount());
           }
           else if(inputText.contains("akaike")){
               inputsTexts = inputText.split("=");
               int index = 0;
               for(int i = 0; i<listBayesianNetworks.size();i++){
                   if(listBayesianNetworksNames.get(i).equals(inputsTexts[1])){
                       index = i;
                       break;
                   }
               }
               System.out.println("El valor de akaike es: " +listBayesianNetworks.get(index).CalculateAkaike());
           }

           else if(inputText.contains("mdl")){
               inputsTexts = inputText.split("=");
               int index = 0;
               for(int i = 0; i<listBayesianNetworks.size();i++){
                   if(listBayesianNetworksNames.get(i).equals(inputsTexts[1])){
                       index = i;
                       break;
                   }
               }
               System.out.println("El valor de MDL es: " + listBayesianNetworks.get(index).CalculateMinDesLength());
           }

           else if(inputText.contains("VerRedBayesiana")){
               System.out.println("Comenzó visualización de red bayesiana");
               inputsTexts = inputText.split("=");
               int index = -1;
               for(int i = 0; i<listBayesianNetworksNames.size();i++){
                   if(listBayesianNetworksNames.get(i).equals(inputsTexts[1])){
                       index = i;
                       break;
                   }
               }
               System.out.println(index);
               bayesianNetwork = listBayesianNetworks.get(index);
               List<String> listFactorNames = bayesianNetwork.GetFactorNames() ;
               List<int[]> listVariablesToEvaluate = bayesianNetwork.GetVariablesToEvaluate();
               List<double[]> listResultsDistributions = bayesianNetwork.GetResultDistributions();
               for(int i = 0; i<listResultsDistributions.size();i++){
                   System.out.println(listFactorNames.get(i));
                   resultsDistribution = listResultsDistributions.get(i);
                  // System.out.println(resultsDistribution.length);
                   PrintFactor(listVariablesToEvaluate.get(i),dataset,bayesianInferecer,resultsDistribution,listFactorNames.get(i));
               }
           }
           else if(inputText.contains("prueba")){
               bayesianNetworkReader = new BayesianNetworkReader();
               bayesianNetworkReader.ReadDAGSFile("C:\\Users\\nilto\\Desktop\\_DAGS\\0.txt");
               //bayesianNetworkReader.GetNextBinaryDag();
           }

           else if(inputText.contains("siguiente")){
               //bayesianNetworkReader.GetNextBinaryDag();
           }

           else if(inputText.contains("CalcularMejorRedBayesiana")) {
               System.out.println("Entro");
               bestEntrophyValue = Double.POSITIVE_INFINITY;
               bestAkaikeValue = Double.POSITIVE_INFINITY;
               currentAkaikeValue = 0;
               currentEntrophyValue = 0;

               bestEntrophyIndex = -100;
               bestAkaikeIndex = -100;

               bayesianNetworkReader = new BayesianNetworkReader();
               //bayesianNetworkReader.ReadDAGSFile("C:\\Users\\nilto\\Desktop\\_DAGS\\Prueba.txt");
               bayesianNetworkReader.ReadDAGSFile("C:\\Users\\nilto\\Desktop\\K2 DAGS\\0.txt");

               Scanner dagScaner = bayesianNetworkReader.GetFileScanner();
               while (dagScaner.hasNextLine()) {
                   dagMatrix = bayesianNetworkReader.GetNextBinaryDag();
                   graph = new Graph();
                   graph.ConstructGraphByMatrix(dagMatrix);
                   bayesianNetwork = new BayesianNetwork(dataset, graph, bayesianInferecer);
                   currentEntrophyValue = bayesianNetwork.CalculateEntropyByCount();
                   currentAkaikeValue = bayesianNetwork.CalculateAkaike();
                   if (bestEntrophyValue > currentEntrophyValue) {
                       bestEntrophyValue = currentEntrophyValue;
                       bestEntrophyIndex = bayesianNetworkReader.GetCurrentDagIndex();
                   }
                   if (bestAkaikeValue > currentAkaikeValue) {
                       bestAkaikeValue = currentAkaikeValue;
                       bestAkaikeIndex = bayesianNetworkReader.GetCurrentDagIndex();
                   }
               }
               System.out.println("La mejor red bayesiana por entropia es: " + bestEntrophyIndex);
               System.out.println("El valor de la mejor entropia es: " + currentEntrophyValue);
               System.out.println("La mejor red bayesiana por akaike es: " + bestAkaikeIndex);
               System.out.println("El valor del mejor akaike es: " + currentAkaikeValue);
           } else if(inputText.contains("matrixconfussion")) {
               System.out.println("Entra");
               inputsTexts = inputText.split("=");
               int index = 0;
               for(int i = 0; i<listBayesianNetworks.size();i++){
                   if(listBayesianNetworksNames.get(i).equals(inputsTexts[1])){
                       index = i;
                       break;
                   }
               }
               bayesianNetwork = listBayesianNetworks.get(index);
                bayesianNetwork.PredictClass();
           }

            else if(inputText.contains("exit")){
                isExecutingProgram = false;
            }
        }

        /*
        long startTime = System.currentTimeMillis();

        String fileName = "src\\dataset2.arff";
        Dataset dataset = new Dataset();
        ARFFReader arffReader = new ARFFReader(fileName,dataset);
        dataset = arffReader.ExecuteAutomaton();
        dataset.ConvertDataListToMatrix();
        dataset.PrintData();


        System.out.println("El número de tuplas es: " + dataset.GetTuplesNumbs());
        int[] variablesIndex = {0};
        int[] variablesValues = {1};
        int result = dataset.CountTuples(variablesIndex,variablesValues);
        //System.out.println("El valor específico es: " + dataset.GetSpecificValue(3,1));
        System.out.println("El número de conteos es: " + result);
*/
        /*
        FrequentistInferencer frequentistInferencer = new FrequentistInferencer(dataset);
        float resultMarginal = frequentistInferencer.CalculateMarginalDistribution(0,1);
        System.out.println("La distribución marginal es: " + resultMarginal);

        int[] v1 = {0,1};
        int[] v2 = {1,1};
        float resultJoint = frequentistInferencer.CalculateJointDistribution(v1,v2);
        System.out.println("La distribución conjunta es: " + resultJoint);

        int[] v3 = {0,1};
        int[] v4 = {1,1};
        float resultConditional = frequentistInferencer.CalculateConditionalDistribution(v3,v4);
        System.out.println("La distribución condicional es: " + resultConditional);
        */


        /*
        BayesianInferecer bayesianInferecer = new BayesianInferecer(dataset,1);
        int v5 = 0;
        double[] resultBayesianMarginal = bayesianInferecer.CalculateMarginalDistribution(v5);
        for(int i = 0; i<resultBayesianMarginal.length;i++){
            System.out.println("La distribución marginal para la variable " + v5
                    + " con valor " + i + " es: " + resultBayesianMarginal[i]);
        }

        int[] v6 = {0,1,2};
        double[] resultBayesianJoint = bayesianInferecer.CalculateJointDistribution2(v6);
        double f = 0;
        for(int i = 0; i<resultBayesianJoint.length;i++){
            f = f + resultBayesianJoint[i];
            System.out.println("La distribución conjunta para las variable " + v6
                    + " con valor " + i + " es: " + resultBayesianJoint[i]);
        }
        System.out.println("La distribución tiene valor: " + f);

        int[] v7 = {1,0};
        double[] resultBayesianConditional = bayesianInferecer.CalculateConditionalDistributionWithNormalization(v7);
        double g = 0;
        for(int i = 0; i<resultBayesianConditional.length;i++){
            g = g + resultBayesianConditional[i];
            System.out.println("La distribución condicional para las variable " + v7
                    + " con valor " + i + " es: " + resultBayesianConditional[i]);
        }
        System.out.println("La distribución tiene valor: " + g);

        long endTime = System.currentTimeMillis();
        //System.out.println("That took " + (endTime - startTime) + " milliseconds");

         */
    }
}
