import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Graph {
    private List<Integer[]> graphConections;
    private String fileName;
    private FileReader fileReader;
    private Scanner fileScanner;
    private String currentLine;
    private String[] currentLineValues;

    public void ReadSaveConections(String file) throws FileNotFoundException {
        graphConections = new ArrayList<Integer[]>();
        fileName = file;
        fileReader = new FileReader(fileName);
        fileScanner = new Scanner(fileReader);
        int currentLineValue = 0;
        while(fileScanner.hasNextLine()) {
            currentLine = fileScanner.nextLine();
            List<Integer> currentConections = new ArrayList<Integer>();
            currentConections.clear();
            currentLineValues = currentLine.split(" ");
            currentConections.add(currentLineValue);
            for(int i = 0; i<currentLineValues.length;i++){
                if(Integer.parseInt(currentLineValues[i])!=0){
                    currentConections.add(i);
                }
            }
            Integer[] conections = new Integer[currentConections.size()];
            conections = currentConections.toArray(conections);
            graphConections.add(conections);
            /*
            if(currentConections.size() != 0) {
                Integer[] conections = new Integer[currentConections.size()];
                conections = currentConections.toArray(conections);
                graphConections.add(conections);
            }else{
                Integer[] conections = new Integer[1];
                conections[0] = -1;
                graphConections.add(conections);
            }
             */
            currentLineValue = currentLineValue + 1;
        }
        for(int i = 0; i<graphConections.size();i++){
            for(int j = 0; j<graphConections.get(i).length;j++){
                System.out.print(graphConections.get(i)[j] + " ");
            }
            System.out.println();
        }
    }
    public void ConstructGraphByMatrix(int[][] matrix) {
        graphConections = new ArrayList<Integer[]>();
        for (int i = 0; i < matrix.length; i++) {
            List<Integer> currentConections = new ArrayList<Integer>();
            currentConections.clear();
            currentConections.add(i);
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] != 0) {
                    currentConections.add(j);
                }
            }

            Integer[] conections = new Integer[currentConections.size()];
            conections = currentConections.toArray(conections);
            graphConections.add(conections);
        }
    }

    public List<Integer[]> GetGraphConnections(){
        return graphConections;
    }
}
