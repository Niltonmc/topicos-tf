import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class BayesianNetworkReader {
    private FileReader fileReader;
    private Scanner fileScanner;
    private String currentLine;
    private String[] currentLineValues;
    private long currentDAGIndex;
    //private int[] binaryDAG;
    private int[][] binaryDAGMatrix;

    public void ReadDAGSFile(String fileName ) throws FileNotFoundException {
        fileReader = new FileReader(fileName);
        fileScanner = new Scanner(fileReader);

    }
    public int[] InvertUsingForInt(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
        return array;
    }

    public void IntToBin(){
        currentLine = Long.toBinaryString(currentDAGIndex);
        //System.out.println(binario.length());
        if(currentLine.length() < 49){
            int len = 49 - currentLine.length();
            for (int i = 0; i < len; i++) {
                currentLine = "0" + currentLine;
            }
            //System.out.println(binario);
            //System.out.println(binario.length());
        }
    }

    public void BinaryToMatrix(){
        binaryDAGMatrix = new int[7][];
        String[] temp;
        int[] tempInt = new int[7];
        if(currentLine.length() == 49){
            temp = currentLine.split("");
            //temp = invertUsingFor(temp);

            //System.out.println(temp.length);
            for (int i = 0; i < 7; i++) {
                tempInt = new int[7];
                for (int j = 0; j < 7; j++) {
                    tempInt[j] = Integer.parseInt(temp[7*i + j]);
                }
                tempInt = InvertUsingForInt(tempInt);
                binaryDAGMatrix[i] = tempInt;
            }
            /*
            System.out.println("-------------");
            for (int i = 0; i < mat.length; i++) {
                for (int j = 0; j < mat[0].length; j++) {
                    System.out.print(mat[i][j] + ",");
                }
                System.out.println();
            }
            System.out.println();
            */
        }
    }

    public int[][] GetNextBinaryDag(){
            currentLine = fileScanner.nextLine();
            currentDAGIndex = Long.parseLong(currentLine);
            IntToBin();
            BinaryToMatrix();
            /*
            for(int i = 0;i<binaryDAGMatrix.length;i++){
                for(int j = 0;j<binaryDAGMatrix[0].length;j++){
                    System.out.print(binaryDAGMatrix[i][j] + "   ");
                }
                System.out.println();
            }
            */

            return binaryDAGMatrix;
            /*
            binaryDAG = new int[bitsQuantity];
            for (int i = bitsQuantity-1; i >= 0; i--, currentDAGIndex /= 2) {
                if (currentDAGIndex % 2 == 0) {
                    binaryDAG[i] = 0;
                }
                if (currentDAGIndex % 2 == 1) {
                    binaryDAG[i] = 1;
                }
            }
            return binaryDAG;
             */
    }

    public Scanner GetFileScanner(){
        return fileScanner;
    }
    public String GetCurrentLine(){
        return currentLine;
    }
    public long GetCurrentDagIndex(){
        return currentDAGIndex;
    }
}
