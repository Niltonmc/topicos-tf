import java.util.ArrayList;
import java.util.List;

public class BayesianNetwork {
    private Dataset dataset;
    private Graph graph;
    private double entropy;
    private BayesianInferecer bayesianInferecer;
    private List<String> listFactorNames;
    private List<int[]> listVariablesToEvaluate;
    private List<double[]> listResultsDistributions;
    private double akaike;
    private  double minDescripLength;
    double[] resultsDistribution;

    private int countForEntrophy;

    private int[] testVariablesIndex;
    private int[] testVariablesValues;
    private int[][] dataTestMatrix;

    private int truePositives;
    private int falsePositives;
    private int falseNegatives;
    private int trueNegatives;
    private int[] predictedClasses;

    private double accuracy;
    private double precision;
    private double recall;
    private double fMeasure;

    public BayesianNetwork(Dataset data, Graph gr, BayesianInferecer inferecer) {
        dataset = data;
        graph = gr;
        bayesianInferecer = inferecer;
        entropy = 0;
        listFactorNames = new ArrayList<String>();
        listResultsDistributions = new ArrayList<double[]>();
        listVariablesToEvaluate = new ArrayList<int[]>();

        predictedClasses = new int[dataset.GetTestDatasetLength()];
    }
    private int[] ConvertIntegerToInt(Integer[] integerArray){
        int[] result = new int[integerArray.length];
        for (int i = 0; i < integerArray.length; i++) {
            result[i] = integerArray[i].intValue();
        }
        return result;
    }
    private String ConstructFactorName(int[] variables){
        String name = "Factor " + dataset.GetVariableName(variables[0]) + ": " + "P("+dataset.GetVariableName(variables[0]);
        for(int i = 1; i<variables.length;i++){
            if(i == 1){
                name = name + "|";
            }
            if(i < (variables.length - 1)){
                name = name + dataset.GetVariableName(variables[i]) + ",";
            }
            if(i == (variables.length - 1)){
                name = name + dataset.GetVariableName(variables[i]);
            }
        }
        name = name +")";
        return name;
    }

    private double CalculateLog(double x, int base) {
        return (Math.log(x) / Math.log(base));
    }

    public double CalculateEntropyByProbabiliy() {
        List<Integer[]> graphConections = graph.GetGraphConnections();
        Integer[] variableToEvaluate;
        for (int i = 0; i < graphConections.size(); i++) {
            variableToEvaluate = graphConections.get(i);
            if(variableToEvaluate.length == 1){
               resultsDistribution = bayesianInferecer.CalculateMarginalDistribution(variableToEvaluate[0]);
                int[] arr = {variableToEvaluate[0]};
                String name = ConstructFactorName(arr);
                listVariablesToEvaluate.add(arr);
                listFactorNames.add(name);
                listResultsDistributions.add(resultsDistribution);
            }else{
                int[] arr = ConvertIntegerToInt(variableToEvaluate);
                /*
                for(int i = 0; i<arr.length;i++){
                    System.out.print(arr[i]);
                }
                System.out.println();
                 */
                resultsDistribution = bayesianInferecer.CalculateConditionalDistributionWithNormalization(arr);
                String name = ConstructFactorName(arr);
                listVariablesToEvaluate.add(arr);
                listFactorNames.add(name);
                listResultsDistributions.add(resultsDistribution);
            }
            for(int j = 0; j<resultsDistribution.length;j++){
                entropy = entropy + resultsDistribution[j] * CalculateLog(resultsDistribution[j],2);
            }
        }
        return entropy;
    }

    public double CalculateEntropyByCount() {
        entropy = 0;
        List<Integer[]> graphConections = graph.GetGraphConnections();
        Integer[] variableToEvaluate;

        int[] distributionCountsForEntrophy;

        for (int i = 0; i < graphConections.size(); i++) {
            //System.out.println(i);
            variableToEvaluate = graphConections.get(i);
            if(variableToEvaluate.length == 1){
                resultsDistribution = bayesianInferecer.CalculateMarginalDistribution(variableToEvaluate[0]);

                distributionCountsForEntrophy = bayesianInferecer.GetDistributionCountForEntrophy();

                int[] arr = {variableToEvaluate[0]};
                String name = ConstructFactorName(arr);
                listVariablesToEvaluate.add(arr);
                listFactorNames.add(name);
                listResultsDistributions.add(resultsDistribution);
            }else{
                int[] arr = ConvertIntegerToInt(variableToEvaluate);
                resultsDistribution = bayesianInferecer.CalculateConditionalDistributionWithNormalization(arr);

                distributionCountsForEntrophy = bayesianInferecer.GetDistributionCountForEntrophy();

                String name = ConstructFactorName(arr);
                listVariablesToEvaluate.add(arr);
                listFactorNames.add(name);
                listResultsDistributions.add(resultsDistribution);
            }
            for(int j = 0; j<resultsDistribution.length;j++){
                entropy = entropy + distributionCountsForEntrophy[j] * CalculateLog(resultsDistribution[j],2);
            }
        }
        return entropy;
    }

    public double CalculateAkaike(){
        int k = 0;
        akaike = 0;
        for(int i = 0; i<listResultsDistributions.size();i++){
            k = k+listResultsDistributions.get(i).length;
        }
        akaike = entropy+k;
        return akaike;
    }

    public double CalculateMinDesLength(){
        double k = 0;
        minDescripLength = 0;
        for(int i = 0; i<listResultsDistributions.size();i++){
            k = k+listResultsDistributions.get(i).length;
            //System.out.println(k);
        }
        minDescripLength = entropy+(k/2)*CalculateLog(dataset.GetTuplesNumbs(),2);
        //System.out.println(CalculateLog(dataset.GetTuplesNumbs(),2));
        //System.out.println(dataset.GetTuplesNumbs());
        return minDescripLength;
    }

    public List<String> GetFactorNames(){
        return listFactorNames;
    }
    public List<int[]> GetVariablesToEvaluate(){
        return listVariablesToEvaluate;
    }
    public List<double[]> GetResultDistributions(){
        return listResultsDistributions;
    }

    public void PredictClass(){
        System.out.println("Entra a predecir clase");
        int testClassIndex = 0;
        double resultDistribution = 0;
        double testResultDistribution = 0;

/*
        for(int i = 0; i<listVariablesToEvaluate.size();i++){
            for(int j = 0;j<listVariablesToEvaluate.get(i).length;j++){
                System.out.print(listVariablesToEvaluate.get(i)[j]);
            }
            System.out.println();
        }
 */

        dataTestMatrix = dataset.GetDataTestMatrix();

        /*
        int tmpClassPos = -1;

        for(int k = 0; k<dataTestMatrix.length;k++) {
            for (int i = 0; i < listVariablesToEvaluate.size(); i++) {
                testVariablesIndex = listVariablesToEvaluate.get(i);
                for(int j = 0; j<testVariablesIndex.length;j++){
                    if(testVariablesIndex[j] == 6){
                        tmpClassPos = j;
                    }
                }
                testVariablesValues = new int[testVariablesIndex.length];
                for (int j = 0; j < listVariablesToEvaluate.get(i).length; j++) {
                    testVariablesValues[j] = dataTestMatrix[i][listVariablesToEvaluate.get(i)[j]];
                }

                for (int j = 0; j < dataset.GetVariableCardinality(6); j++) {
                    testVariablesValues[0] = j;
                    resultDistribution = bayesianInferecer.CalculateSpecificalConditionalDistributionWithNormalization(
                            testVariablesIndex, testVariablesValues);
                    if (resultDistribution > testResultDistribution) {
                        testResultDistribution = resultDistribution;
                        testClassIndex = j;
                    }
                }
                predictedClasses[i] = testClassIndex;
            }
        }
*/
        // STANDAR NORMAL

        testVariablesIndex = new int[dataTestMatrix[0].length];
        for(int i = 0;i<testVariablesIndex.length;i++){
            testVariablesIndex[i] = i;
        }
        testVariablesValues = new int[dataTestMatrix[0].length];
        for(int i = 0; i<dataTestMatrix.length;i++) {
            for (int j = 0; j < (dataTestMatrix[0].length - 1); j++) {
                testVariablesValues[j+1] = dataTestMatrix[i][j];
            }
            for (int k = 0; k < dataset.GetVariableCardinality(6); k++) {
                testVariablesValues[0] = k;
                resultDistribution = bayesianInferecer.CalculateSpecificalConditionalDistributionWithNormalization(
                        testVariablesIndex, testVariablesValues);
                if (resultDistribution > testResultDistribution) {
                    testResultDistribution = resultDistribution;
                    testClassIndex = k;
                }
            }
            predictedClasses[i] = testClassIndex;
        }
        //STANDAR NORMAL
        ConstructConfussionMatrix();
    }
    private void ConstructConfussionMatrix(){
        System.out.println("Entra a calcular matriz de confusion");
        for(int i = 0;i<predictedClasses.length;i++){
            if(dataTestMatrix[i][6] == 1){
                if(dataTestMatrix[i][6] == predictedClasses[i]){
                    truePositives = truePositives+1;
                }else{
                    falseNegatives = falseNegatives+1;
                }
            }else if(dataTestMatrix[i][6] == 0){
                if(dataTestMatrix[i][6] == predictedClasses[i]){
                    trueNegatives = trueNegatives+1;
                }else{
                    falsePositives = falsePositives+1;
                }
            }
        }
        CalculateQualityMetrics();
    }
    public void CalculateQualityMetrics(){
        System.out.println("Entra a mostrar métricas de calidad");
        //ConstructConfussionMatrix();
        accuracy = (double)(trueNegatives+truePositives)/(double)predictedClasses.length;
        precision = (double)(truePositives)/(double)(falsePositives+truePositives);
        recall = (double)(truePositives)/(double)(falseNegatives+truePositives);
        fMeasure = 2*((precision*recall)/(precision+recall));
        System.out.print(truePositives + "\t" + falsePositives);
        System.out.println();
        System.out.println(falseNegatives + "\t" + trueNegatives);
        System.out.println("La acuraccy es: " + accuracy);
        System.out.println("La precision es: " + precision);
        System.out.println("El recall es: " + recall);
        System.out.println("El fMeasure es: " + fMeasure);
    }
}
