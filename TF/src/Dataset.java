import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Dataset {
    private String datasetName;
    private List<String> variableNamesList;
    private List<String[]> variablesOriginalValuesList;
    private List<Integer> variablesCardinalityList;
    private List<int[]> dataList;
    private int[][] dataMatrix;
    private int[][] dataTrainingMatrix;
    private int[][] dataTestMatrix;
    private int crossValidationFolds;

    public Dataset() {
        variableNamesList = new ArrayList<String>();
        variablesOriginalValuesList = new ArrayList<String[]>();
        variablesCardinalityList = new ArrayList<Integer>();
        dataList = new ArrayList<int[]>();
        crossValidationFolds = 10;
    }
    public void SetDatasetName(String name){
        datasetName = name;
    }
    public void SetVariableName(String name){
        System.out.println("Nombre variable: " + name);
        variableNamesList.add(name);
    }
    public void SetVariableValues(String[] values){
        variablesOriginalValuesList.add(values);
        variablesCardinalityList.add(values.length);
        System.out.println("Longitud variable: " + values.length);
    }
    public void PrintVariableValues(){
        for(int i = 0; i<variablesOriginalValuesList.size();i++){
            System.out.println(Arrays.toString(variablesOriginalValuesList.get(i)));
        }
    }
    public int TransformVariableToInt(int pos, String value){
        int val = Arrays.asList(variablesOriginalValuesList.get(pos)).indexOf(value);
        return val;
    }
    public void AddDataToList(int[] values){
        dataList.add(values);
    }
    public void ConvertDataListToMatrix() {
        dataMatrix = new int[dataList.size()][];
        dataMatrix = dataList.toArray(dataMatrix);
    }
    public void SetDataMatrix(int[][] dataValues){
        dataMatrix = dataValues;
    }

    public void CrossValidationMatrix(int currentFold){
        int foldPositions = dataMatrix.length/crossValidationFolds;
        int startPos = foldPositions*(currentFold-1);
        int finalPos = foldPositions*currentFold;
        dataTrainingMatrix = new int[dataMatrix.length - foldPositions][dataMatrix[0].length];
        dataTestMatrix = new int[foldPositions][dataMatrix[0].length];
        for(int i = 0; i<foldPositions;i++){
            for (int j = 0; j < dataMatrix[0].length; j++) {
                dataTestMatrix[i][j] =dataMatrix[startPos+i][j];
            }
        }
        //System.out.println(dataMatrix.length);
        //System.out.println(dataTestMatrix.length);
        int varPos = 0;
        for(int i = 0; i<dataMatrix.length;i++){
            if(i < startPos || (i>(finalPos))) {
                for (int j = 0; j < dataMatrix[0].length; j++) {
                    dataTrainingMatrix[varPos][j] = dataMatrix[i][j];
                    //dataTrainingMatrix[varPos][j] = 0;
                }
                varPos = varPos + 1;
                //System.out.println(varPos);
            }
        }


        for (int i = 0; i < dataTrainingMatrix.length; i++) {
            for (int j = 0; j < dataTrainingMatrix[0].length; j++) {
                System.out.print(dataTrainingMatrix[i][j]+"\t");
            }
            System.out.println();
        }

        //System.out.println(foldPositions);
    }

    public void PrintData(){
        for (int i = 0; i < dataMatrix.length; i++) {
            for (int j = 0; j < dataMatrix[0].length; j++) {
                System.out.print(dataMatrix[i][j]+"\t");
            }
            System.out.println();
        }
    }
    public int GetTuplesNumbs(){
        return dataTestMatrix.length;
        //return dataMatrix.length;
    }
    public int GetSpecificValue(int row, int column){
        return dataTestMatrix[row][column];
        //return dataMatrix[row][column];
    }

    public int CountTuples(int[] variablesIndex, int[] variablesValues) {
        if (variablesIndex.length != variablesValues.length) {
            return 0;
        }
        for (int j = 0; j < variablesIndex.length; j++) {
            if (variablesIndex[j] >= dataTestMatrix[1].length || variablesIndex[j] < 0) {
            //if (variablesIndex[j] >= dataMatrix[1].length || variablesIndex[j] < 0) {
                return 0;
            }
        }
        int nTuples = GetTuplesNumbs();
        int countTuples = 0;
        for (int i = 0; i < nTuples; i++) {
            boolean continueResearching = true;
            for (int j = 0; j < variablesIndex.length; j++) {
                if (variablesValues[j] == GetSpecificValue(i, variablesIndex[j]) && continueResearching) {
                    continueResearching = true;
                }else{
                    continueResearching = false;
                    break;
                }
            }
            if(continueResearching){
                countTuples = countTuples + 1;
            }
        }
        return countTuples;
    }

    public int GetVariableCardinality(int index){
        return variablesCardinalityList.get(index);
    }

    public int GetVariableIndex(String name){
        for(int i = 0; i <variableNamesList.size();i++){
            if(variableNamesList.get(i).equals(name)){
                return i;
            }
        }
        return -1;
    }
    public int GetVariableValue(String name, int varIndex){
        for(int i = 0; i <variablesOriginalValuesList.get(varIndex).length;i++){
            if(variablesOriginalValuesList.get(varIndex)[i].equals(name)){
                return i;
            }
        }
        return -1;
    }
    public String GetVariableValueName(int varIndex, int pos){
        return variablesOriginalValuesList.get(varIndex)[pos];
    }
    public String GetVariableName(int varIndex){
        return variableNamesList.get(varIndex);
    }

    public int GetTestDatasetLength(){
        return dataTestMatrix.length;
    }
    public int[][] GetDataTestMatrix(){
        return dataTestMatrix;
    }
}
