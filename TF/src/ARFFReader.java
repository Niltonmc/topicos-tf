import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.TreeMap;

public class ARFFReader {

    // VARIABLES PARA LECTURA DE DATOS
    private String fileName;
    private FileReader fileReader;
    private Scanner fileScanner;
    private String currentLine;
    private String[] currentLineValues;

    // VARIABLES PARA EL AUTÓMATA
    private String currentState;

    // VARIABLES PERTENECIENTES A OTRAS CLASES
    private Dataset dataset;

    // VARIABLES NECESARIAS PARA EL HASH MAP
    /*
    private TreeMap<String, Integer> hashMapValues;
    private int variableIndex = 0;
    private int totalTuples = 0;
    private int[][] dataMatrix;
    private int currentTuple = 0;
     */

    public ARFFReader(){

    }

    public ARFFReader(String file, Dataset data) throws FileNotFoundException {
        fileName = file;
        fileReader = new FileReader(fileName);
        fileScanner = new Scanner(fileReader);
        //hashMapValues = new TreeMap<String, Integer>(); // USADO PARA EL HASH MAP
        currentState = "LecturaNombre";
        dataset = data;
        //totalTuples = CountTotalTuples(); // USADO PARA EL HASH MAP
    }

    public String[] ReplaceDifferentCharacters(String line){
        line = line.replaceAll("\\{"," ");
        line = line.replaceAll(","," ");
        line = line.replaceAll("\\}"," ");
        return line.split(" ");
    }

    /* // USADO PARA EL HASH MAP
    public int CountTotalTuples() throws FileNotFoundException {
        int numbTuples = 0;
        while(fileScanner.hasNextLine()){
            currentLine = fileScanner.nextLine();
            if(!currentLine.contains("@")){
                numbTuples = numbTuples+1;
            }
        }
        fileScanner.close();
        System.out.println(numbTuples);
        return numbTuples;
    }
    */ // USADO PARA EL HASH MAP

    public Dataset ExecuteAutomaton() throws FileNotFoundException {
        //fileReader = new FileReader(fileName); // USADO PARA EL HASH MAP
        //fileScanner = new Scanner(fileReader); // USADO PARA EL HASH MAP

        while(fileScanner.hasNextLine()){
            currentLine = fileScanner.nextLine();
            switch (currentState){
                case "LecturaNombre":
                    if(currentLine.contains("@relation")){
                        currentState = "LecturaVariables";
                        currentLineValues = currentLine.split(" ");
                        dataset.SetDatasetName(currentLineValues[1]);
                        System.out.println("Dataset=["+currentLineValues[1]+"]");
                    }
                    break;
                case "LecturaVariables":
                    if(currentLine.contains("@attribute")){
                        currentLineValues = ReplaceDifferentCharacters(currentLine);
                        dataset.SetVariableName(currentLineValues[1]);
                        String[] variableValues = new String[currentLineValues.length-2];
                        for (int i = 2; i < currentLineValues.length; i++) {
                            variableValues[i - 2] = currentLineValues[i];
                            //hashMapValues.put(variableIndex + "-" + currentLineValues[i], (i-2)); // USADO PARA EL HASH MAP
                        }
                        //variableIndex = variableIndex+1; // USADO PARA EL HASH MAP
                        dataset.SetVariableValues(variableValues);
                    }else if(currentLine.contains("@data")){
                        //currentState = "CrearMatrizDatos"; // USADO PARA EL HASH MAP
                        currentState = "LecturaDatos";
                    }
                    break;
                    /* // USADO PARA EL HASH MAP
                case "CrearMatrizDatos":
                    dataMatrix = new int[totalTuples][variableIndex];
                    currentLineValues = currentLine.split(",");
                    for (int i = 0; i < currentLineValues.length; i++) {
                        int indice = hashMapValues.get(i+"-"+currentLineValues[i]);
                        dataMatrix[currentTuple][i] = indice;
                    }
                    currentTuple = currentTuple + 1;
                    currentState = "LecturaDatos";
                    break;
                     */ // USADO PARA EL HASH MAP
                case "LecturaDatos":

                    currentLineValues = currentLine.split(",");
                    int[] numericValues = new int[currentLineValues.length];
                    for(int i =0;i<currentLineValues.length;i++){
                        numericValues[i] = dataset.TransformVariableToInt(i,currentLineValues[i]);
                    }
                    dataset.AddDataToList(numericValues);

                    /* // USADO PARA EL HASH MAP
                    currentLineValues = currentLine.split(",");
                    for (int i = 0; i < currentLineValues.length; i++) {
                        int indice = hashMapValues.get(i+"-"+currentLineValues[i]);
                        dataMatrix[currentTuple][i] = indice;
                    }
                    currentTuple = currentTuple + 1;
                     */ // USADO PARA EL HASH MAP
                    break;
            }
        }
        currentState = "TerminoLectura";
        fileScanner.close();
        //dataset.SetDataMatrix(dataMatrix); // USADO PARA EL HASH MAP
        return dataset;
    }
}
